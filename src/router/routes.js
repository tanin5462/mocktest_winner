const routes = [
  // {
  // path: '/',
  // component: () => import('layouts/MyLayout.vue'),
  // children: [{
  //   path: '',
  //   component: () => import('pages/Index.vue')
  // }]
  // }
  {
    path: '',
    component: () => import('pages/login.vue'),
    name: 'login'
  },
  {
    path: '/main',
    component: () => import('pages/main.vue'),
    name: 'main'
  }, {
    path: '/exam',
    component: () => import('pages/mocktest.vue'),
    name: 'exam'
  }, {
    path: '/result',
    component: () => import('pages/result.vue'),
    name: 'result'
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
